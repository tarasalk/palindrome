<?php

require ('palindrome.php');

$aText = [
    'Аргентина манит негра',
    'Sum summus mus',
    'Просто тестовая строка',
    'абвгд еж'
];

$aPalindrome = [];
foreach ($aText as $palindrome) {
    $item['text'] = $palindrome;
    $item['isPalindrom'] = Palindrome::isPalindrome($palindrome);
    $item['longestPalindrome'] = Palindrome::findLongestPalindrome($palindrome);

    $aPalindrome[] = $item;
}
?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<table class="table" style="width: 1000px;">
    <tr>
        <th>текст</th>
        <th>палиндром</th>
        <th>максимальный подпалиндром</th>
    </tr>
    <? foreach ($aPalindrome as $palindrome) {?>
        <tr>
            <td><?=$palindrome['text']?></td>
            <td><?=$palindrome['isPalindrom'] ? 'Да':'Нет'?></td>
            <td><?=$palindrome['longestPalindrome']?></td>
        </tr>
    <? } ?>
</table>


