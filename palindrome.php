<?php 

class Palindrome {
    public static function isPalindrome($originalText) {
        $text = str_replace(' ', '', $originalText);
        if (mb_strlen ($text) < 2) return false;

        $text = mb_strtolower($text);
        $reverseText = self::utf8_strrev($text);

        return $text == $reverseText;
    }

    public static function findLongestPalindrome($text) {
        $textArray = preg_split("//u", $text, -1, PREG_SPLIT_NO_EMPTY);
        $textLength = count($textArray);

        for ($i = 0; $i < $textLength; $i++) {
            for ($j = $textLength; $j > 0; $j--) {
                $tempPalindrome = mb_substr($text, $i, $j - $i);
                if (self::isPalindrome($tempPalindrome)) {
                    return $tempPalindrome;
                }
            }
        }

        return false;
    }

    private static function utf8_strrev($str){
        preg_match_all('/./us', $str, $ar);
        return join('', array_reverse($ar[0]));
    }
}

